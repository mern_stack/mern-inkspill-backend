const { validationResult } = require("express-validator");
const mongoose = require("mongoose");

// models
const HttpError = require("../models/http-error");
const User = require("../models/user");
const Post = require("../models/post");
const Comment = require("../models/comment");

// functions
async function getCommentsByPostId(req, res, next) {
  const postId = req.params.pid;

  const pageSize = 10;
  let page = req.query.page;

  let comments;
  let totalCount = await Comment.countDocuments({});
  console.log("totalCount: " + totalCount);
  const totalPages = Math.ceil(totalCount / pageSize);

  try {
    comments = await Comment.find({ postId }, null, {
      skip: (page - 1) * pageSize,
      limit: pageSize,
      sort: { createdAt: -1 },
    });
  } catch (err) {
    console.error(err);
    return next(
      new HttpError("Error occurred while accessing the database.", 500)
    );
  }

  if (!comments) {
    return next(new HttpError("Comments do not exist.", 404));
  }

  let commentsDTO = [];
  for (comment of comments) {
    let c = comment.toObject({ getters: true });

    let commentAuthor = await User.findById(c.authorId, "username userImage");
    c.author = {};
    c.author.username = commentAuthor.username;
    c.author.img = commentAuthor.userImage;
    commentsDTO.push(c);
  }

  res.json({
    comments: commentsDTO,
    totalPages,
  });
}

async function createPostComment(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.error(errors);
    return next(
      new HttpError("Invalid inputs detected, please check your data.", 422)
    );
  }

  const { text } = req.body;
  const postId = req.params.pid;
  const authorId = req.userData.id;

  let user;
  try {
    user = await User.findById(authorId);
  } catch (err) {
    return next(
      new HttpError(
        "Failed to retrieve authorId from database, please try again.",
        500
      )
    );
  }
  if (!user) {
    return next(new HttpError(`User id ${authorId} does not exist!`, 404));
  }

  let post;
  try {
    post = await Post.findById(postId);
  } catch (err) {
    return next(
      new HttpError(
        "Failed to retrieve postId from database, please try again.",
        500
      )
    );
  }
  if (!post) {
    return next(new HttpError(`Post id ${postId} does not exist!`, 404))
  }

  const newComment = new Comment({
    text,
    authorId,
    postId,
  });

  try {
    // transaction rollbacks if an error occurs
    const session = await mongoose.startSession();
    session.startTransaction();
    await newComment.save({ session });
    await session.commitTransaction();
  } catch (err) {
    console.error(err);
    return next(
      new HttpError(
        "Failed to create comment in database, please try again.",
        500
      )
    );
  }

  res.status(201).json({ comment: newComment });
}

exports.createPostComment = createPostComment;
exports.getCommentsByPostId = getCommentsByPostId;