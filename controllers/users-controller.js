const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const cloudinary = require("cloudinary").v2;

// models
const HttpError = require('../models/http-error');
const User = require('../models/user');

// bcrypt
const bcrypt = require('bcrypt');
const SALTROUNDS = 10;

async function getUsers(req, res, next) {
    let users;
    try {
        users = await User.find({}, '-password -email');
    } catch(err) {
        return next(new HttpError('Error occurred while accessing the database.', 500));
    }

    if (!users) {
        return next(new HttpError('Users do not exist.', 404));
    }

    res.json({ users: users.map(user => user.toObject({ getters: true })) });
}

async function getUserById(req, res, next) {
    const uid = req.params.uid;

    let user;
    try {
        user = await User.findById(uid, '-password');
    } catch(err) {
        return next(new HttpError('Error occurred while accessing user database.', 500));
    }

    if (!user) {
        return next(new HttpError(`User of id ${uid} does not exist.`, 404));
    }

    res.json({ user: user.toObject({ getters: true })});
}

async function signUp(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next(new HttpError('Invalid inputs detected, please check your data.', 422));
    }

    const { username, email, password } = req.body;
    const localImagePath = req.file.path;
    
    let existingUser;
    try {
        existingUser = await User.findOne({ email });
    } catch(err) {
        return next(new HttpError('Signup failed; please try again later.', 500));
    }

    if (existingUser) {
        return next(new HttpError('User with email already exists.', 422));
    }

    try {
        existingUser = await User.findOne({ username });
    } catch(err) {
        return next(new HttpError('Signup failed; please try again later.', 500));
    }

    if (existingUser) {
        return next(new HttpError('Username already exists.', 422));
    }

    let hashedPassword;
    try {
      hashedPassword = await bcrypt.hash(password, SALTROUNDS);
    } catch (err) {
      const error = new HttpError('Could not create user, please try again.', 500);
      return next(error);
    }

    let imageUrl;
    await cloudinary.uploader.upload(localImagePath, function (error, result) {
      if (error) {
        console.error(error);
        return next(
          new HttpError(`An error occurred when uploading the image.`),
          500
        );
      }
      imageUrl = result.secure_url;
    });
    
    const newUser = new User({
        username,
        email,
        password: hashedPassword,
        userImage: imageUrl,
    });
    console.log("Created New User: ");
    console.log(newUser);

    try {
        await newUser.save();
    } catch (err) {
        console.log(err);
        return next(new HttpError('Signup failed; please try again later.', 500));
    }
    
    let token;
    try {
        token = jwt.sign(
            {userId: newUser.id, email: newUser.email}, 
            process.env.JWT_TOKEN_KEY,
            { expiresIn: '1h' }
        );
    } catch (err) {
        console.log(err);
        return next(new HttpError('Signup failed; please try again later.', 500));
    }

    res.status(201).json({
        user: {
            id: newUser.id,
            username: newUser.username,
            email: newUser.email,
            token
        }
    });
}

async function signIn(req, res, next) {
    const { email, password } = req.body;

    let user;
    try {
        user = await User.findOne({email});
    } catch(err) {
        return next(new HttpError('Signin failed: please try again later.', 500));
    }

    if (!user) {
        return next(new HttpError('Signin failed: incorrect email or password.', 401));
    }

    // test a matching password
    let isMatch = false;
    try {
        isMatch = await bcrypt.compare(password, user.password);
    } catch (err) {
        const error = new HttpError('Could not log you in, please check your credentials and try again.', 500);
        return next(error);
    }

    if (isMatch) {
        let token;
        try {
            token = jwt.sign(
                {id: user.id, email: user.email}, 
                process.env.JWT_TOKEN_KEY,
                { expiresIn: '1h' }
            );
        } catch (err) {
            return next(new HttpError('Signup failed, please try again later.', 500));
        }

        res.status(200).json({
            user: {
                id: user.id,
                username: user.username,
                email: user.email,
                token
            }
        });
    } else {
        return next(new HttpError('Signin failed: incorrect email or password.', 401));
    }
}

exports.getUsers = getUsers;
exports.getUserById = getUserById;
exports.signUp = signUp;
exports.signIn = signIn;