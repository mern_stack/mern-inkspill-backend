const fs = require("fs");

const { validationResult } = require("express-validator");
const mongoose = require("mongoose");
const cloudinary = require("cloudinary").v2;

// models
const HttpError = require("../models/http-error");
const User = require("../models/user");
const Post = require("../models/post");
const Comment = require("../models/comment");

// services
const PostService = require("../services/PostService");

// functions
async function getPosts(req, res, next) {
  const pageSize = 5;

  const params = {
    page: req.query.page,
    pageSize,
  };

  const [totalPages, err1] = await PostService.getTotalPages(pageSize);
  const [posts, err2] = await PostService.getPosts(params);

  if (err1) return next(err1);
  if (err2) return next(err2);

  res.json({
    posts: posts.map((post) => post.toObject({ getters: true })),
    totalPages,
  });
}

async function getPostById(req, res, next) {
  const pid = req.params.pid;

  const [post, err] = await PostService.getPostById(pid);
  
  if (err) return next(err);

  res.json({
    post: post.toObject({ getters: true }),
  });
}

async function getPostsByUserId(req, res, next) {
  const uid = req.params.uid;

  let posts;
  try {
    posts = await Post.find({ authorId: uid });
  } catch (err) {
    return next(
      new HttpError("Error occurred while accessing the database.", 500)
    );
  }

  if (posts.length === 0) {
    return next(
      new HttpError(`Could not find posts for specified user id ${uid}.`, 404)
    );
  }

  res.json({ posts: posts.map((post) => post.toObject({ getters: true })) });
}

async function createPost(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.error(errors);
    return next(
      new HttpError("Invalid inputs detected, please check your data.", 422)
    );
  }

  const { title, description } = req.body;
  const authorId = req.userData.id;
  const localImagePath = req.file.path;

  let user;
  try {
    user = await User.findById(authorId);
  } catch (err) {
    return next(
      new HttpError(
        "Failed to retrieve authorId from database, please try again.",
        500
      )
    );
  }

  if (!user) {
    return next(new HttpError(`User id ${authorId} does not exist!`, 404));
  }

  let imageUrl;
  await cloudinary.uploader.upload(localImagePath, function (error, result) {
    if (error) {
      console.error(error);
      return next(
        new HttpError(`An error occurred when uploading the image.`),
        500
      );
    }
    console.log(result);
    console.log(result.secure_url);
    imageUrl = result.secure_url;
  });

  const newPost = new Post({
    title,
    description,
    authorId,
    postImage: imageUrl,
  });

  try {
    // transaction rollbacks if an error occurs
    const session = await mongoose.startSession();
    session.startTransaction();
    await newPost.save({ session });
    await session.commitTransaction();
    fs.unlink(localImagePath, (err) => {
      console.error(err);
    });
  } catch (err) {
    console.error(err);
    return next(
      new HttpError("Failed to create post in database, please try again.", 500)
    );
  }

  res.status(201).json({ post: newPost.toObject({ getters: true }) });
}

async function updatePostById(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs detected, please check your data.", 422)
    );
  }

  const pid = req.params.pid;
  const { title, description } = req.body;

  let post;
  try {
    post = await Post.findById(pid);
  } catch (err) {
    return next(
      new HttpError("Error occurred while accessing the database.", 500)
    );
  }
  if (!post) {
    return next(
      new HttpError(`Could not find post for specified id ${pid}.`, 404)
    );
  }

  if (post.authorId.toString() !== req.userData.id) {
    return next(new HttpError("Unauthorized to modify this resource.", 401));
  }

  post.title = title;
  post.description = description;

  try {
    await post.save();
  } catch (err) {
    console.error(err);
    return next(
      new HttpError("Error occurred while updating the database.", 500)
    );
  }

  res.status(200).json({ post: post.toObject({ getters: true }) });
}

async function deletePostById(req, res, next) {
  const pid = req.params.pid;

  let post;
  try {
    post = await Post.findById(pid);
  } catch (err) {
    return next(
      new HttpError("Error occurred while accessing the database.", 500)
    );
  }
  if (!post) {
    return next(
      new HttpError(
        `Could not delete post for specified id ${pid}; does not exist.`,
        404
      )
    );
  }

  if (post.authorId.toString() !== req.userData.id) {
    return next(
      new HttpError("You are unauthorized to modify this resource.", 401)
    );
  }

  const imagePath = post.postImage;
  try {
    // transaction rollbacks if an error occurs
    const session = await mongoose.startSession();
    session.startTransaction();
    await post.remove({ session });
    await session.commitTransaction();
  } catch (err) {
    console.error(err);
    return next(
      new HttpError("Error occurred while removing entry from database.", 500)
    );
  }
  fs.unlink(imagePath, (err) => {
    console.error(err);
  });

  res.status(200).json({ message: `Deleted post ${pid}.` });
}

exports.getPosts = getPosts;
exports.getPostById = getPostById;
exports.getPostsByUserId = getPostsByUserId;
exports.createPost = createPost;
exports.updatePostById = updatePostById;
exports.deletePostById = deletePostById;