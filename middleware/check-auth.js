const HttpError = require("../models/http-error");
const jwt = require('jsonwebtoken');

const checkAuth = (req, res, next) => {
    // prevent OPTIONS request from being blocked, so that ensuing requests can continue
    if (req.method === 'OPTIONS') {
        return next();
    }

    try {
        const token = req.headers.authorization.split(' ')[1]; // Authorization: 'Bearer TOKEN'
        if (!token) {
            throw new Error('Authentication failed: no token found.');
        }

        // if verification fails, this will throw an error
        const decodedToken = jwt.verify(token, process.env.JWT_TOKEN_KEY);
        
        req.userData = { id: decodedToken.id };
        // verification success: allow request to continue
        next();

    } catch (err) {
        return next(new HttpError('Authentication failed: no token found.', 401));
    }
}

module.exports = checkAuth;