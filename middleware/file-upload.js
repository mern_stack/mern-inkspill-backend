const multer  = require('multer');
const { v4: uuidv4 } = require('uuid');

const MIME_TYPE_MAP = {
    'image/jpg': 'jpg',
    'image/jpeg': 'jpeg',
    'image/png': 'png',
};

const fileUpload = multer({
    limit: 2000000, // 2MB max
    storage: multer.diskStorage({
        destination: (req, file, callback) => {
            callback(null, 'uploads/images');
        },
        filename: (req, file, callback) => {
            const fileExtension = MIME_TYPE_MAP[file.mimetype];
            callback(null, uuidv4() + '.' + fileExtension);
        }
    }),
    fileFilter: (req, file, callback) => {
        const isValid = !!MIME_TYPE_MAP[file.mimetype]; // converts undefined to false, true to true
        const error = isValid ? null : new Error('Invalid Mimetype!');
        callback(error, isValid);
    }
});

module.exports = fileUpload;