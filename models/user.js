const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      minlength: 6,
    },
    userImage: {
      type: String,
      required: true,
    },
  },
  { timestamps: { createdAt: true, updatedAt: false } }
);

// // update values on new document in a pre save hook
// userSchema.pre("save", function (next) {
//   if (!this.createdAt) this.createdAt = new Date();
//   next();
// });

// Apply the uniqueValidator plugin to userSchema
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);
