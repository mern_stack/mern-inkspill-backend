const mongoose = require("mongoose");

const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    authorId: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "User",
    },
    postImage: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

postSchema.index({ authorId: 1, title: 1 }, { unique: true });

module.exports = mongoose.model("Post", postSchema);
