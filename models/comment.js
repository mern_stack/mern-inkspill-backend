const mongoose = require("mongoose");

const commentSchema = new mongoose.Schema(
  {
    text: {
      type: String,
      required: true,
    },
    authorId: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "User",
    },
    postId: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Post",
    },
  },
  { timestamps: true }
);

commentSchema.index({ authorId: 1, postId: 1 }, { unique: false });

module.exports = mongoose.model("Comment", commentSchema);
