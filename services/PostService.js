// models
const HttpError = require("../models/http-error");
const Post = require("../models/post");

async function getTotalPages(pageSize) {
  let totalCount;
  try {
    totalCount = await Post.countDocuments({});
  } catch (err) {
    return [
      null,
      new HttpError("Error occurred while accessing the database.", 500),
    ];
  }

  const totalPages = Math.ceil(totalCount / pageSize);
  return [totalPages, null];
}

async function getPosts(params) {
  let posts;
  const { page, pageSize } = params;

  try {
    posts = await Post.find({}, null, {
      skip: (page - 1) * pageSize,
      limit: pageSize,
      sort: { createdAt: -1 },
    });
  } catch (err) {
    return [
      null,
      new HttpError("Error occurred while accessing the database.", 500),
    ];
  }

  if (!posts) {
    return [null, new HttpError("Posts do not exist.", 404)];
  }

  return [posts, null];
}

async function getPostById(pid) {
  let post;
  try {
    post = await Post.findById(pid);
  } catch (err) {
    return [
      null,
      new HttpError("Error occurred while accessing the database.", 500),
    ];
  }

  if (!post) {
    return [
      null,
      new HttpError(`Could not find post for specified id ${pid}.`, 404),
    ];
  }

  return [post, null];
}

module.exports.getTotalPages = getTotalPages;
module.exports.getPosts = getPosts;
module.exports.getPostById = getPostById;
