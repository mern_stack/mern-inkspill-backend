export default function debugLog(message) {
    if (process.env.DEBUG_MODE) {
        console.log(message);
    }
}