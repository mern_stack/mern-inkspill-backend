const fs = require("fs");
const path = require("path");

const cors = require("cors");
const express = require("express");
const dotenv = require("dotenv").config();
const mongoose = require("mongoose");
const cloudinary = require("./config/cloudinary-config");
const bodyParser = require("body-parser");

// models
const HttpError = require("./models/http-error");

// routes
const postsRouter = require("./routes/posts-routes");
const usersRouter = require("./routes/users-routes");

const app = express();

// CORS cross-origin resource sharing
// app.use(cors());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});

app.use(bodyParser.json()); // parse json data into javascript object, then pass to next middleware

// static routes
app.use("/uploads/images", express.static(path.join("uploads", "images")));

// router middleware
app.use("/api/posts", postsRouter);
app.use("/api/users", usersRouter);

// middleware only executed when route does not exist
app.use((req, res, next) => {
  return next(new HttpError("Route does not exist.", 404));
});

// middleware only executed on error requests
app.use((error, req, res, next) => {
  // on error, delete file that was created
  if (req.file) {
    fs.unlink(req.file.path, (err) => {
      console.error(err);
    });
  }

  if (res.headerSent) {
    return next(error);
  }

  console.log(error);
  // res.status(error.code || 500);
  res.status(500);
  res.json({ message: error.message || "An unknown error occurred." });
});

// mongoose config
mongoose
  .connect(`${process.env.DB_CONNECTION_STRING}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    app.listen(process.env.PORT || 5000, function () {
      console.log("Server running on Heroku/localhost:5000");
    });
  })
  .catch((err) => {
    console.log(err);
  });
