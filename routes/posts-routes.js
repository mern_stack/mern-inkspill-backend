const express = require("express");
const { check } = require("express-validator");

// middlewares
const fileUpload = require("../middleware/file-upload");
const checkAuth = require("../middleware/check-auth");

// controller
const postsController = require("../controllers/posts-controller");
const commentsController = require("../controllers/comments-controller");

const router = express.Router();

router.get("/", postsController.getPosts);

router.get("/:pid", postsController.getPostById);

router.get("/user/:uid", postsController.getPostsByUserId);

router.get("/:pid/comments", commentsController.getCommentsByPostId);

// authentication, authorization middleware
router.use(checkAuth);

router.post(
  "/",
  fileUpload.single("postImage"),
  [check("title").notEmpty(), check("description").isLength({ min: 5 })],
  postsController.createPost
);

router.post(
  "/:pid/comments",
  [check("text").notEmpty().isLength({ max: 500 })],
  commentsController.createPostComment
);

router.put(
  "/:pid",
  [check("title").notEmpty(), check("description").isLength({ min: 5 })],
  postsController.updatePostById
);

router.delete("/:pid", postsController.deletePostById);

module.exports = router;
