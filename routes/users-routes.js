const express = require('express');
const { check } = require('express-validator');

// file-upload
const fileUpload = require('../middleware/file-upload');

// controller
const usersController = require('../controllers/users-controller');

const router = express.Router();

router.get('/', usersController.getUsers);

router.get('/:uid', usersController.getUserById);

router.post(
    '/signup',
    fileUpload.single('userImage'),
    [
        check('username').notEmpty(),
        check('email').normalizeEmail().isEmail(),
        check('password').isLength({min: 6})
    ],
    usersController.signUp
);

router.post('/signin', usersController.signIn);

module.exports = router;